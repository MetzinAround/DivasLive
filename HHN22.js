console.log("Don't go Alone...");

 if (process.env.NODE_ENV === "develop") {
  require("dotenv").config();
};

const schedule1 = require("node-schedule");
const rule1 = new schedule1.RecurrenceRule();
rule1.hour = 11;
rule1.minute = 20;
rule1.tz = "Etc/GMT+4";


var Twit = require('twit');

var config = require('./configPj.js');

var T = new Twit(config);

var hello = "sup"

function welcomeToTheFog(){

  let date1 = new Date("2022/09/02");
  let date2 = new Date();
  let date3 = new Date("2022/10/31");
  let differenceInTime = date1.getTime() - date2.getTime();
  let timeLeft = date3.getTime() - date2.getTime();
  let dayCounter = Math.ceil(differenceInTime / (1000 * 3600 * 24));
  let daysLeft = Math.ceil(timeLeft / (1000 * 3600 * 24)); 
 

  const fogTweet = `It's almost time 🕰. ${dayCounter} days until @HorrorNightsORL is back. Don't go alone. 😳 https://www.halloweenhorrornights.com/`;
  const oneDayMore = `Can you hear it? The screams? 😈 @HorrorNightsORL is just around the corner... https://www.halloweenhorrornights.com/ `;
  const todaysTheDay = `We've finally arrived. 😈 @HorrorNightsORL is opening up the gates tonight. https://www.halloweenhorrornights.com/`;
  const theFogHasArrived = `Your time is running out ⏱ . @HorrorNightORL is back for only ${daysLeft} days. Join us before it's too late. https://www.halloweenhorrornights.com/`;
  const finalGirl = `One day Left of @HorrorNightsORL. Will you join us in the fog? Or will you wait until next year? https://www.halloweenhorrornights.com/`;

  let postStatus = "";

  if (dayCounter >= 2 ) {
    postStatus = fogTweet;
  } else if (dayCounter == 0) {
    postStatus = todaysTheDay;
  } else if (dayCounter == 1) {
    postStatus = oneDayMore;
  } else if (daysLeft == 1) {
    postStatus = finalGirl;
  } else if (daysLeft >= 1) {
    postStatus = theFogHasArrived;
  } else if (daysLeft <= 0 && dayCounter < -1) {
    postStatus = "Until next year... 👻";
  } else {
    postStatus = "This shouldn't be happening";
  }

  console.log(`Tweeting: ${postStatus}`);

   T.post('statuses/update', {status: postStatus}, function (err, data, response)
      { 
        if(err){
          console.log(err);
         } else {
      console.log(data.text);
       }
      }); 
}

const job3 = schedule1.scheduleJob(rule1, welcomeToTheFog);

