module.exports = { 
    consumer_key: process.env.consumer_key_brit, 
    consumer_secret: process.env.consumer_secret_brit,
    access_token: process.env.access_token_brit,
    access_token_secret: process.env.access_token_secret_brit,
};