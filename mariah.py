from matplotlib.pyplot import install_repl_displayhook
import tweepy
import config_Mariah as conf
import re
import schedule as sched
import time


print("I don't want a lot for Christmas...")
#Schedule is mon-Fri at 6:30 am ETC

auth = tweepy.OAuth1UserHandler(conf.consumer_key, conf.consumer_secret, conf.access_token, conf.access_token_secret
)

api = tweepy.API(auth) 
# create class that handles stream
class LogTweets(tweepy.Stream):
        def on_status(self, tweet):
          #id and name from tweet object
          id = tweet.id_str
          name = tweet.user.screen_name
          #covers getting extended tweets to check the text      
          try:
            tweet = tweet.extended_tweet["full_text"]
          except AttributeError:
            tweet = tweet.text
          #regex to see if emojis were used or creates empty list
          regex_list = re.search("👀🙄😒") or []
          #if list isn't empty, then regex worked. 
          sweet_fantasy = (len(regex_list) > 0)

          #regex worked and tweet contains Mechanical Carey's name, reply text gets created and sent as an update
          if (sweet_fantasy is True) and ("@MechanicalCarey" in tweet):

             replyText = "@"+ name + "... I don't know her ¯\_(ツ)_/¯..."

             api.update_status(replyText, id)
          else:
            print("Just a Sweet Fantasy")

#creates instance of LogTweets with authentication
stream = LogTweets(conf.consumer_key, conf.consumer_secret, conf.access_token, conf.access_token_secret)


#hashtags as str in list will be watched live on twitter. 

stream.filter(track="@MechanicalCarey")




def AlwaysBeMyBaby():
    print("You can't escape me")
    api.update_status("Boy don't you know you can't escape me 😘  https://www.youtube.com/watch?v=Dxce3s7bV9s") 

sched.every().day.at("11:00").do(AlwaysBeMyBaby)

while True:
  sched.run_pending()
  time.sleep(1)


